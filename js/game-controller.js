'use strict';

// глобальные переменные
var g_keyboardListener = new KeyboardListener();
var g_world = new World();

window.onload = function() {
    var context = document.getElementById("gameCanvas").getContext('2d');
    var map = new Map1();
    var informer = new Informer();
    var userTank = new Tank();
    g_keyboardListener.AddFunctionPerformEvent(userTank.UpdateKeyboardState);

    g_world.AddDynamicObject(userTank);
    g_world.AddStaticObjects(map.GetGrassObjects());

    setInterval(UpdateGame, 100);

    function UpdateGame() {
        informer.NextTick();
        g_world.Render(context);
        informer.Render(context);
    }
};
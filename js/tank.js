'use strict';

function Tank() {
    var _this = this;
    _this.OBJECT_TYPE = 'tank';

    // состояние танка
    _this.angle = 0;
    _this.position = {
        x: 0,
        y: 0
    };

    _this.ChangeAngle = function() {
        if (g_keyboardListener.isKeyUpPress)     _this.angle = g_world.ROUTE.UP;
        if (g_keyboardListener.isKeyRightPress)  _this.angle = g_world.ROUTE.RIGHT;
        if (g_keyboardListener.isKeyDownPress)   _this.angle = g_world.ROUTE.DOWN;
        if (g_keyboardListener.isKeyLeftPress)   _this.angle = g_world.ROUTE.LEFT;
    };

    _this.UpdateKeyboardState = function() {
        _this.ChangeAngle();
    };

    _this.Render = function (context) {
        canvasDrawLib.drawRotateImg(context, imageStorage.tank, _this.position, _this.angle);
    };
}

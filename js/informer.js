'use strict';

function Informer() {
    this.tickNumber = 0;

    this.NextTick = function() {
        this.tickNumber++
    };
    this.Render = function(ctx) {
        ctx.fillStyle = "#000";
        ctx.strokeStyle = "#000";
        ctx.font = "14px Arial";

        var timeStr = "Кол. тиков: " + this.tickNumber;
        ctx.strokeText(timeStr, 10, 25);
    };
}
'use strict';

function Map1() {
    var _this = this;

    _this.width = 7;           // ширина игрового поля
    _this.height = 5;          // высота игрового поля
    _this.grassMap = [         // карта трава
        [1, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1]
    ];

    _this.GetGrassObjects = function() {
        var grassObjects = [];
        for (var y = 0; y < _this.height; y++) {
            for (var x = 0; x < _this.width; x++) {
                if (_this.grassMap[y][x]) {
                    var grass = new Grass();
                    grass.position.x = ((x * g_world.OBJECTS_SIZE.CELL) + (g_world.OBJECTS_SIZE.CELL / 2));
                    grass.position.y = ((y * g_world.OBJECTS_SIZE.CELL) + (g_world.OBJECTS_SIZE.CELL / 2));
                    grassObjects.push(grass);
                }
            }
        }
        return grassObjects;
    };
}
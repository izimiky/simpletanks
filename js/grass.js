'use strict';

function Grass() {
    var _this = this;
    _this.OBJECT_TYPE = 'grass';

    // состояние танка
    _this.angle = 0;
    _this.position = {
        x: 0,
        y: 0
    };

    _this.Render = function (context) {
        canvasDrawLib.drawRotateImg(context, imageStorage.grass, _this.position, _this.angle);
    };
}

'use strict';

function ImageStorage() {
    this.grass = new Image();
    this.grass.src = "images/grass.png";

    this.tank = new Image();
    this.tank.src = "images/tank.png";

    this.fire = new Image();
    this.fire.src = "images/fire.png";
}

var imageStorage = new ImageStorage();
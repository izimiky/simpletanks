'use strict';

var canvasDrawLib = {
    TO_RADIANS: (Math.PI / 180),

    drawRotateImg: function(context, image, position, angle) {
        context.save();
        context.translate(position.x, position.y);
        context.rotate(angle * this.TO_RADIANS);
        context.drawImage(image, -(image.width / 2), -(image.height / 2));
        context.restore();
    }
};
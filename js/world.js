'use strict';

function World() {
    var _this = this;

    // константы мира
    _this.ROUTE = {
        UP: 0,
        RIGHT: 90,
        DOWN: 180,
        LEFT: 270
    };
    _this.OBJECTS_SIZE = {
        CELL: 70
    };

    // переменные мира
    var dynamicObjects = [];
    var staticObjects = [];

    _this.AddDynamicObject = function (addObj) {
        dynamicObjects.push(addObj);
    };

    _this.AddStaticObjects = function (addObjects) {
        addObjects.forEach(function(addObj) {
            staticObjects.push(addObj);
        });
    };

    _this.Render = function (context) {
        context.clearRect(0, 0, 999, 999);
        staticObjects.forEach(function (obj) {
            obj.Render(context);
        });
        dynamicObjects.forEach(function (obj) {
            obj.Render(context);
        });
    };
}
'use strict';

function KeyboardListener() {
    var _this = this;

    var KEY_CODE = {
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40
    };

    this.isKeyLeftPress = false;
    this.isKeyUpPress = false;
    this.isKeyRightPress = false;
    this.isKeyDownPress = false;
    var performFunctions = []; // функции, нуждающиеся в выполнении после события нажатия клавиш управления

    this.UpdateKeyboardListener = function(e) {
        var isChange = true;

        switch (e.keyCode) {
            case KEY_CODE.LEFT:
                _this.isKeyLeftPress = (e.type == 'keydown');
                break;
            case KEY_CODE.UP:
                _this.isKeyUpPress = (e.type == 'keydown');
                break;
            case KEY_CODE.RIGHT:
                _this.isKeyRightPress = (e.type == 'keydown');
                break;
            case KEY_CODE.DOWN:
                _this.isKeyDownPress = (e.type == 'keydown');
                break;
            default:
                isChange = false;
        }

        if (isChange) {
            performFunctions.forEach(function(performFunction) {
                performFunction();
            });
        }
    };

    this.AddFunctionPerformEvent = function(performFunction) {
        performFunctions.push(performFunction);
    };

    window.addEventListener('keydown', this.UpdateKeyboardListener, false);
    window.addEventListener('keyup', this.UpdateKeyboardListener, false);
}